import { Link } from "react-router-dom";
import { FaPlus } from "react-icons/fa6";

const AddButton = () => {
  return (
    <Link to="/note/new" className="floating-button">
      <FaPlus />
    </Link>
  );
};

export default AddButton;
