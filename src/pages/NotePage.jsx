import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
// import notes from "../assets/data";
import { FaChevronLeft } from "react-icons/fa6";

const NotePage = () => {
  const [note, setNote] = useState(null);
  const [initialNoteBody, setInitialNoteBody] = useState("");
  const { id: noteId } = useParams();
  const navigate = useNavigate();
  // const note = notes.find(note => note.id === Number(noteId));

  useEffect(() => {
    const getNote = async () => {
      if (noteId === "new") return;
      const response = await fetch(`http://localhost:8000/notes/${noteId}`);
      const data = await response.json();
      setNote(data);
      setInitialNoteBody(data.body);
    };

    getNote();
  }, [noteId]);

  const createNote = async () => {
    await fetch(`http://localhost:8000/notes`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ ...note, updated: new Date() })
    });
  };

  const updateNote = async () => {
    await fetch(`http://localhost:8000/notes/${noteId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ ...note, updated: new Date() })
    });
  };

  const deleteNote = async () => {
    await fetch(`http://localhost:8000/notes/${noteId}`, {
      method: "DELETE"
    });
    navigate("/");
  };

  const handleSubmit = () => {
    if (noteId !== "new" && !note.body) {
      deleteNote();
    } else if (noteId !== "new" && note.body !== initialNoteBody) {
      updateNote();
    } else if (noteId === "new" && note !== null) {
      createNote();
    }

    navigate("/");
  };

  const handleNoteBodyChange = e => {
    setNote({ ...note, body: e.target.value });
  };

  return (
    <div className="note">
      <div className="note-header">
        <h3>
          <FaChevronLeft onClick={handleSubmit} />
        </h3>

        {noteId !== "new" ? (
          <button onClick={deleteNote} className="delete-btn">
            Delete
          </button>
        ) : (
          <button onClick={handleSubmit}>Done</button>
        )}
      </div>
      <textarea
        spellCheck="false"
        value={note?.body}
        onChange={handleNoteBodyChange}></textarea>
    </div>
  );
};

export default NotePage;
