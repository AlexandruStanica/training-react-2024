import { useEffect, useState } from "react";
// import notes from "../assets/data";
import ListItem from "../components/ListItem";
import { FaNoteSticky } from "react-icons/fa6";
import AddButton from "../components/AddButton";

const NotesListPage = () => {
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    const getNotes = async () => {
      const response = await fetch(`http://localhost:8000/notes`);
      const data = await response.json();
      setNotes(data);
    };

    getNotes();
  }, []);

  return (
    <div className="notes">
      <div className="notes-header">
        <h2 className="notes-title">
          <FaNoteSticky /> Notes
        </h2>
        <p className="notes-count">{notes.length}</p>
      </div>
      <div className="notes-list">
        {notes.map(note => (
          <ListItem key={note.id} note={note} />
        ))}
      </div>
      <AddButton />
    </div>
  );
};

export default NotesListPage;
